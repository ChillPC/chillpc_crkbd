#pragma once

#include "config_common.h"

#define MANUFACTURER ChillPC
#define PRODUCT chillpc_crkbd
#define VENDOR_ID    0xEEEE
#define PRODUCT_ID   0x0001
#define DEVICE_VER   0x0001

/* #define MASTER_LEFT */
/* #define SPLIT_USB_DETECT */
/* #define EE_HANDS */

#define USE_SERIAL
#define SOFT_SERIAL_PIN D2

#define DIODE_DIRECTION COL2ROW

#define MATRIX_ROWS  8
#define MATRIX_COLS  6
#define MATRIX_ROW_PINS { F4, F5, F6, F7 }
#define MATRIX_COL_PINS { D1, D0, D4, C6, D7, E6 }

#define MATRIX_ROW_PINS_RIGHT { D1, D0, D4, C6 }
#define MATRIX_COL_PINS_RIGHT { F4, F5, F6, F7, B1, B3}

/* Debounce reduces chatter (unintended double-presses) - set 0 if debouncing is not needed */
#define DEBOUNCE 5

/* define if matrix has ghost (lacks anti-ghosting diodes) */
//#define MATRIX_HAS_GHOST

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE


/* disable these deprecated features by default */
#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION
